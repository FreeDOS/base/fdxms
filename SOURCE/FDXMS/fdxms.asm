//-----------------------------------------------------------------------------
//		F R E E - D O S		X M S - D R I V E R
//-----------------------------------------------------------------------------
//
// Improvments by Martin Str�mberg.
// Copyright 2001-2005, Martin Str�mberg.
// 
// Martin can be reached at <ams@ludd.ltu.se>. Please start the subject line 
// with "FDXMS". If you have a bug report, use bugtrack on the FreeDOS site,
// <http://www.freedos.org>. Make sure to include the FDXMS.SYS version number.
//
//
//-----------------------------------------------------------------------------
// Written by Till Gerken for the Free-DOS project.
//
// If you would like to use parts of this driver in one of your projects, please
// check up with me first.
//
// Till can be reached at:	till@tantalo.net (Internet)
//
// Comments and bug reports are always appreciated.
//
// Copyright (c) 1995, Till Gerken
//-----------------------------------------------------------------------------
// -- IMPLEMENTATION NOTES --
//
// - The Protected Mode handling is very simple, it fits only for the least
//   needs
// - I didn't care about reentrancy. If this thing should be used in
//   Multitasking Environments, well, somebody got to take a look at the code.
// - Function 0Bh (Move Block) uses it's own Protected Mode handling.
//   It doesn't provide Interrupt windows.
// - Some ideas were taken from the original XMS driver written by
//   Mark E. Huss (meh@bis.adp.com), but everything has been completely
//   rewritten, so if there are bugs, they are mine, not his. ;)
//-----------------------------------------------------------------------------

	.arch i8086 // jumps			// We start in 8086 mode.
	
#include "fdxms.inc"	// this file contains structures,
			// constants and the like

//-----------------------------------------------------------------------------
// 16-bit resident code and data
//-----------------------------------------------------------------------------

	.section .text
	.code16

//-----------------------------------------------------------------------------
// device driver header

	.long	-1			// last driver in list
	.short	0x8000			// driver flags
	.short	strategy		// pointer to strategy routine
	.short	interrupt		// pointer to interrupt handler
	.ascii	"XMSXXXX0"		// device driver name

//-----------------------------------------------------------------------------
// global data

gdt32:	
	.short	gdt_size,dummy,0
dummy:	
	.quad	0
code16dsc:	
	.byte	0xff,0xff,0,0,0,0x9a,0,0
core32dsc:	
	.byte	0xff,0xff,0,0,0,0x92,0xcf,0
gdt_size=.-dummy

code16idx	=	0x08
core32idx	=	0x10

bios_gdt:
	.byte	0,0,0,0,0,0,0,0		// Dummy entries for BIOS.
	.byte	0,0,0,0,0,0,0,0		// Dummy entries for BIOS.
	.short	0xffff			// Bits 0-15 of source segment length.
bios_src_low:	
	.short	0			// Bits 0-15 of source address.
bios_src_middle:	
	.byte	0			// Bits 16-23 of source address.
	.byte	0x93			// Source access rights.
	.byte	0x0f			// More type bits and bits 16-19 of source segment length.
bios_src_high:	
	.byte	0			// Bits 24-31 of source address.
	.short	0xffff			// Bits 0-15 of dest. segment length.
bios_dst_low:		
	.short	0			// Bits 0-15 of dest. address.
bios_dst_middle:	
	.byte	0			// Bits 16-23 of dest. address.
	.byte	0x93			// Dest. access rights.
	.byte	0x0f			// More type bits and bits 16-19 of dest segment length.
bios_dst_high:	
	.byte	0			// Bits 24-31 of dest. address.
	.byte	0,0,0,0,0,0,0,0		// Dummy entries for BIOS.
	.byte	0,0,0,0,0,0,0,0		// Dummy entries for BIOS.
	

a20_locks:	
	.short	0		// internal A20 lock count
flags:	
	.byte	0
hma_min:	
	.short	0		// minimal space in HMA that
				// has to be requested
int15_mem:	
	.short	0		// The amount of memory reserved for
				// old applications.		

old_int15:	
	.long	0		// old INT15h vector
old_int2f:	
	.long	0		// old INT2fh vector

request_ptr:	
	.long	0		// pointer to request header

xms_delay:	
	.short	1		// Delay calls after A20 line has been toggled.
xms_num_handles:	
	.short	32		// number of available handles
	
	
#ifdef XXMS
#include "xxms_variables.inc"
#endif // XXMS
	
#ifdef TRACE_CODE
trace:	
	.long	0
#endif // TRACE_CODE

#ifdef DEBUG_CODE
debug:	
	.byte	0
#endif // DEBUG_CODE
	
//-----------------------------------------------------------------------------
// strategy routine. is called by DOS to initialize the driver once.
// only thing to be done here is to store the address of the device driver
// request block.
// In:	ES:BX - address of request header
// Out:	nothing

strategy:	
	movw	%es, %cs:request_ptr+2	// store segment addr
	mov	%bx, %cs:request_ptr	// store offset addr
	lret				// far return here!

//-----------------------------------------------------------------------------
// interrupt routine. called by DOS right after the strategy routine to
// process the incoming job. also used to initialize the driver.

interrupt:	
	push	%di
	push	%ds

	lds	%cs:request_ptr, %di	// Load address of request header.
	cmpb	$CMD_ISTATUS, request_hdr_cmd(%di)	// Input status?
	je	2f
	cmpb	$CMD_OSTATUS, request_hdr_cmd(%di)	// Output status?
	je	2f
	cmpb	$CMD_INIT, request_hdr_cmd(%di)	// Do we have to initialize?
	jne	3f
	testb	$FLAG_INITIALISED, %cs:flags	// Have we initialized already?
	jnz	3f
	call	initialize		// No, do it now!
1:
	pop	%ds
	pop	%di
	lret				// far return here!
2:
	movw	$STATUS_DONE, request_hdr_status(%di)
	jmp	1b
3:
	movw	$(STATUS_ERROR|STATUS_DONE|ERROR_BAD_CMD), request_hdr_status(%di)
	jmp	1b
	
	.arch i386				// Turn on 386 instructions.
	
//-----------------------------------------------------------------------------
// just delays a bit
//	AL - destroyed.

delay:	
delay_ret_patch = .
1:
	in	$0x64, %al
	jmp	2f
2:	
	and	$2, %al
	jnz	1b
	ret

//-----------------------------------------------------------------------------
// enables the A20 address line
//	AL - destroyed.
//	CX - destroyed.
enable_a20:	
	mov	$0xd1, %al
enable_a20_out_value_patch = .-1
	out	%al, $0x64
enable_a20_out_port_patch = .-1
enable_a20_nop_start = .
	call	delay
	mov	$0xdf, %al
	out	%al, $0x60
	call	delay
	mov	$0xff, %al
	out	%al, $0x64
enable_a20_nop_end = .
	mov	%cs:xms_delay, %cx
1:	
	call	delay
	loop	1b
	
	ret

//-----------------------------------------------------------------------------
// disables the A20 address line
//	AL - destroyed.
//	CX - destroyed.

disable_a20:	
	mov	$0xd1, %al
disable_a20_out_value_patch = .-1
	out	%al, $0x64
disable_a20_out_port_patch = .-1
disable_a20_nop_start = .
	call	delay
	mov	$0xdd, %al
	out	%al, $0x60
	call	delay
	mov	$0xff, %al
	out	%al, $0x64
disable_a20_nop_end = .
	mov	%cs:xms_delay, %cx
1:	
	call	delay
	loop	1b
	
	ret

//-----------------------------------------------------------------------------
// tests if the A20 address line is enabled.
// compares 256 bytes at 0:0 with ffffh:10h
// Out:	ZF=0 - A20 enabled
//	ZF=1 - A20 disabled

test_a20:	
	push	%cx
	push	%si
	push	%di
	push	%ds
	push	%es

	xor	%si, %si
	mov	%si, %ds

	pushw	$0xffff
	pop	%es
	mov	$0x10, %di

	mov	$(0x100/4), %cx
	rep	
	cmpsl

	pop	%es
	pop	%ds
	pop	%di
	pop	%si
	pop	%cx
	ret

//-----------------------------------------------------------------------------
// checks if VDISK is already installed
// In:	CS=DS
// Out:	ZF=0 -> VDISK not installed or HMA is or have been allocated.
//	ZF=1 -> VDISK is installed.
//	DI - destroyed
vdisk_id:	
	.ascii	"VDISK"
VDISK_ID_LEN = . - vdisk_id
#define VDISK_ID_INT19_OFS	0x12
#define VDISK_ID_HMA_OFS	0x13

check_vdisk:	
	push	%ax
	push	%cx
	push	%si
	push	%es

	testb	$(FLAG_HMA_USED|FLAG_NO_VDISK), flags
	jnz	1f //@@done
	
	pushw	$0		// Get interrupt vector 19h segment address.
	pop	%es
	mov	%es:0x19*4+2, %es

	// INT19h check.
	mov	$VDISK_ID_INT19_OFS, %di
	mov	$vdisk_id, %si
	mov	$VDISK_ID_LEN, %cx
	rep	
	cmpsb				// Is VDISK here?
	jz	1f //@@done

	// HMA check.
	call	test_a20
	lahf
	jnz	2f // a20_is_on
	
	call	enable_a20

2:	// a20_is_on
	pushw	$0xffff
	pop	%es
	mov	$VDISK_ID_HMA_OFS, %di
	mov	$vdisk_id, %si
	mov	$VDISK_ID_LEN, %cx
	rep	
	cmpsb				// Is VDISK here?
	pushf
	sahf
	jnz	3f // a20_was_on
	
	call	disable_a20

3:	// a20_was_on
	popf

1:	//@@done:	
	pop	%es
	pop	%si
	pop	%cx
	pop	%ax
	ret

//-----------------------------------------------------------------------------
// Interrupt handlers
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// new INT15h handler

int15_handler:	
	cmp	$0x87, %ah		// is it a block move request?
	je	1f
	cmp	$0x88, %ah		// is it a ext. mem size req.?
	je	8f
	cmp	$0xe801, %ax		// Is it a ext. mem size req.?
	je	6f
	ljmp	*%cs:old_int15		// jump to old handler
1:	
	orb	$FLAG_A20_STATE, %cs:flags	// Assume enabled
	call	test_a20		// check if A20 is on or off
	jnz	3f

	andb	$~FLAG_A20_STATE, %cs:flags
3:
	pushf				// simulate INT call
	lcall	*%cs:old_int15
	pushf				// Save flags for return.
	push	%ax
	push	%cx
	testb	$FLAG_A20_STATE, %cs:flags	// see if A20 has to be switched
	jz	4f
	call	enable_a20
	jmp	5f
4:
	call	disable_a20
5:
	pop	%cx
	pop	%ax
	popf
	lret	$2
6:
//	clc - Not necessary as cmp before je cleared carry flag.
	pushf
	lcall	*%cs:old_int15
	jnc	7f
	
	xor	%cx, %cx
	xor	%dx, %dx
7:	
	xor	%bx, %bx		// No memory above 16MiB available.
	// Fall through.
8:
	mov	%cs:int15_mem, %ax	// Fill in what is left over.
	clc
	sti
	lret	$2	

//-----------------------------------------------------------------------------
// new INT2Fh handler. Catches Func. 0x4300+0x4310

int2f_handler:	
	cmp	$0x4300, %ax		// is it "Installation Check"?
	jne	1f
	mov	$0x80, %al		// yes, we are installed //)
	iret
1:
	cmp	$0x4310, %ax		// is it "Get Driver Address"?
	jne	2f
	push	%cs
	pop	%es
	mov	$xms_dispatcher, %bx
	iret
2:
	ljmp	*%cs:old_int2f		// jump to old handler

//-----------------------------------------------------------------------------
// XMS functions
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// returns XMS version number
// In:	AH=0
// Out:	C_AX=XMS version number
//	C_BX=internal revision number
//	C_DX=1 if HMA exists, 0 if not

xms_get_version:	
	movw	$INTERFACE_VER, C_AX
	movw	$DRIVER_VER, C_BX
	movw	$1, C_DX		// HMA is always available.
	ret

//-----------------------------------------------------------------------------
// requests HMA
// In:	AH=1
//	CL=0
//	DX=space needed in HMA (0ffffh if application tries to request HMA)
// Out:	if not successful:	
//	  AX - destroyed
//	  BL=80h -> function not implemented (implemented here //) )
//	  BL=81h -> VDISK is detected
//	  BL=90h -> HMA does not exist
//	  BL=91h -> HMA already in use
//	  BL=92h -> DX less than HMA_MIN

xms_request_hma:	
	testb	$FLAG_HMA_USED, flags	// Is HMA already used?
	movb	$XMS_HMA_IN_USE, %bl
	jnz	xrh_err
	cmp	hma_min, %dx		// Is request big enough?
	movb	$XMS_HMAREQ_TOO_SMALL, %bl
	jb	xrh_err
	orb	$(FLAG_HMA_USED|FLAG_NO_VDISK), flags	// Assign HMA to caller.
	ret
xrh_err:
	pop	%ax
	ret

//-----------------------------------------------------------------------------
// releases HMA
// In:	AH=2
//	CL=0
// Out:	if not successful:	
//	  AX - destroyed
//	  BL=80h -> function not implemented
//	  BL=81h -> VDISK is detected
//	  BL=90h -> HMA does not exist
//	  BL=93h -> HMA was not allocated

xms_release_hma:	
	testb	$FLAG_HMA_USED, flags	// Is HMA used?
	movb	$XMS_HMA_NOT_USED, %bl
	jz	xrh_err
	andb	$~FLAG_HMA_USED, flags	// Now release it.
	ret

//-----------------------------------------------------------------------------
// global A20 address line enable
// In:	AH=3
// Out:	if not successful:
//	  AX - destroyed
//	  BL=80h -> function is not implemented
//	  BL=81h -> VDISK is detected
//	  BL=82h -> A20 failure

xms_global_enable_a20:	
	call	enable_a20		// enable A20
	call	test_a20		// is it really enabled?
	jz	xge_a20_err
	ret
xge_a20_err:
	mov	$XMS_A20_FAILURE, %bl
	pop	%ax
	ret

//-----------------------------------------------------------------------------
// global A20 address line disable
// In:	AH=4
// Out:	if not successful:	
//	  AX - destroyed
//	  BL=80h -> function is not implemented
//	  BL=81h -> VDISK is detected
//	  BL=82h -> A20 failure
//	  BL=84h -> A20 still enabled

xms_global_disable_a20:	
	call	disable_a20		// disable A20
	call	test_a20		// is it really disabled?
	jnz	xge_a20_err
	ret

//-----------------------------------------------------------------------------
// enables A20 locally
// In:	AH=5
// Out:	if not successful:
//	  AX - destroyed
//	  BL=80h -> function not implemented
//	  BL=81h -> VDISK is detected
//	  BL=82h -> A20 failure

xms_local_enable_a20:	
	incw	a20_locks		// increase lock counter
	call	enable_a20		// enable it
	call	test_a20		// test if it is really enabled
	jz	xge_a20_err
	ret

//-----------------------------------------------------------------------------
// disables A20 locally
// In:	AH=6
// Out:	if not successful:
//	  AX - destroyed
//	  BL=80h -> function not implemented
//	  BL=81h -> VDISK is detected
//	  BL=82h -> A20 failure

xms_local_disable_a20:	
	decw	a20_locks		// decrease lock counter
	jnz	1f			// disable only if needed
	call	disable_a20		// disable it
	call	test_a20		// test if it is really disabled
	jnz	xge_a20_err
1:
	ret

//-----------------------------------------------------------------------------
// returns the state of A20
// In:	AH=7
// Out:	C_AX=1 if A20 is physically enabled, C_AX=0 if not
//	C_BL=00h -> function was successful
//	(BL=80h -> function is not implemented)
//	(BL=81h -> VDISK is detected)

xms_query_a20:
	xor	%bl, %bl
	mov	%bl, C_BL
	call	test_a20
	jz	1f
	ret
1:
	pop	%ax
	ret


//-----------------------------------------------------------------------------
// searches a/next free XMS memory block
// In:	DS=CS
//	BX - offset of start handle (if search is continued)
//	CX - remaining handles (if search is continued)
// Out:	CY=1 - no free block
//	  BX - offset of end of handle table
//	CY=0 - free block found
//	  BX - offset of free handle
//	  CX - number of remaining handles

xms_find_free_block:	
	mov	$driver_end, %bx	// start at the beginning of the table
	mov	xms_num_handles, %cx	// check all handles
1:	//@@find_free_block:
	cmpb	$0, xms_handle_used(%bx)	// is it used?
	jnz	xms_find_next_free_block// yes, go on
#ifndef XXMS
	cmpw	$0, xms_handle_xbase(%bx)	// assigned memory block or just blank?
#else
	cmpl	$0, xms_handle_xbase(%bx)	// assigned memory block or just blank?
#endif
	jnz	2f //@@found_block	// assigned, return it
xms_find_next_free_block:
	add	$SIZE_xms_handle, %bx	// skip to next handle
	loop	1b //@@find_free_block	// check next handle
	stc				// no free block found, error
	ret
2:
	clc				// no error, return
	ret

//-----------------------------------------------------------------------------
// searches a/next free XMS memory handle
// In:	DS=CS
//	BX - offset of start handle (if search is continued)
//	CX - remaining handles (if search is continued)
// Out:	CY=1 - no free handle
//	  BX - offset of end of handle table
//	CY=0 - free handle found
//	  BX - offset of free handle
//	  CX - number of remaining handles

xms_find_free_handle:	
	mov	$driver_end, %bx	// start at the beginning of the table
	mov	xms_num_handles, %cx	// check all handles
1:	//@@find_free_handle:
	cmpb	$0, xms_handle_used(%bx)	// is it used?
	jnz	xms_find_next_free_handle	// yes, go on
#ifndef XXMS
	cmpw	$0, xms_handle_xbase(%bx)		// really blank handle?
#else
	cmpl	$0, xms_handle_xbase(%bx)		// really blank handle?
#endif
	jz	2f //@@found_handle	// found a blank handle
xms_find_next_free_handle:
	add	$SIZE_xms_handle, %bx	// skip to next handle
	loop	1b //@@find_free_handle	// check next handle
	stc				// no free block found, error
	ret
2:	//@@found_handle:
	clc				// no error, return
	ret

//-----------------------------------------------------------------------------
// Verifies that a handle is valid and in use.
// In:	DX - handle to verify
// Out:	CY=1 - not a valid handle
//	  BL=0xa2 - XMS_INVALID_HANDLE
//	CY=0 - valid handle
//  AX=modified
	
xms_handle_valid:	
	push	%bx
	mov	%dx, %ax
	sub	$driver_end, %ax	// Is the handle below start of handles?
	jb	1f //@@not_valid
	
	push	%dx
	xor	%dx, %dx
	mov	$SIZE_xms_handle, %bx
	div	%bx
	test	%dx, %dx		// Is the handle aligned on a handle boundary?
	pop	%dx
	jnz	1f //@@not_valid
	
	cmp	xms_num_handles, %ax	// Is the handle number less than the number of handles?
	jae	1f //@@not_valid
	
	// If we come here, the handle is a valid handle
	mov	%dx, %bx
	cmpb	$1, xms_handle_used(%bx)	// Is the handle in use?
	jb	1f //@@not_valid

	// Handle is valid.
//	clc - Not necessary as cmpb before jb cleared carry flag.
	pop	%bx
	ret
	
1:	//@@not_valid:
	// Handle is not valid
	pop	%bx
	mov	$XMS_INVALID_HANDLE, %bl
	stc
	ret
	
//-----------------------------------------------------------------------------
// calculates the move address
// In:	BX - handle (0 if EDX should be interpreted as seg:ofs value)
//	ECX - length
//	EDX - offset
// Out:	CY=0 - valid input (handle).
//	  EBX - absolute move address
//	CY=1 - not valid handle or invalid length.
//	  BL=XMS_INVALID_DESTINATION_HANDLE - not valid handle.
//	  BL=XMS_INVALID_LENGTH - invalid length.
//	
// Modifies: EAX, EDX

xms_get_move_addr:	
	test	%bx, %bx		// translate address in EDX?
	jnz	1f //@@dont_translate
	movzwl	%dx, %eax		// save offset
	xor	%dx, %dx		// clear lower word
	shr	$12, %edx		// convert segment to absolute address
	addr32	lea	(%edx,%eax), %ebx
	
	// Assert length + offset < 0xffff:ffff + 1 = 0x10fff0
	//  "lea	eax, [ ebx + ecx ]" is no good in case offset = 0xfffffffe or so...
	mov	%ebx, %eax
	add	%ecx, %eax
	jc	4f //@@invalid_length
	
	cmp	$0x10fff0, %eax
	ja	4f //@@invalid_length
	
	clc
	ret
	
1:	//@@dont_translate:
	xchg	%dx, %bx		// Now handle is in DX and lower 16 bits of offset in BX.
	call	xms_handle_valid
	jc	2f //@@not_valid_handle

	push	%si
	mov	%dx, %si		// Put handle in SI.
	mov	%bx, %dx		// Get lower 16 bits of length back.
	
	// Assert length + offset <= size.
	//  "lea eax, [ ecx+edx+1024-1 ]" is no good in case ECX or EDX 
	//  equal 0xffffffe or something similarly large.
	mov     %ecx, %eax		// Load eax with sum of ecx
	add     %edx, %eax		// and edx,
	jc      3f //@@invalid_length_pop_si		// (no overflow is permissible)
	add     $(1024-1), %eax		// and add on 2^10-1 for ...
	jc      3f //@@invalid_length_pop_si
	
	shr	$10, %eax		// ... shift so eax is size in KiB.
	
#ifndef XXMS
	test	$0xffff0000, %eax
	jnz	3f //@@invalid_length_pop_si

	cmp	xms_handle_xsize(%si), %ax
	ja	3f //@@invalid_length_pop_si
	
	// Now everything is ok.
	movzwl	xms_handle_xbase(%si), %ebx	// Get block base address.
#else // IFDEF XXMS
	cmp	xms_handle_xsize(%si), %eax
	ja	3f //@@invalid_length_pop_si
	
	// Now everything is ok.
	mov	xms_handle_xbase(%si), %ebx	// Get block base address.
#endif // XXMS	

	shl	$10, %ebx		// Convert from kb to absolute.
	add	%edx, %ebx		// Add offset into block.
	pop	%si
	ret
		
2:	//@@not_valid_handle:	
	mov	$XMS_INVALID_DESTINATION_HANDLE, %bl
	ret

3:	//@@invalid_length_pop_si:
	pop	%si
4:	//@@invalid_length:
	mov	$XMS_INVALID_LENGTH, %bl
	stc
	ret	

//-----------------------------------------------------------------------------
// frees an XMS block
// In:	AH=0ah
//	DX=handle to allocated block that should be freed
// Out:	AX=1 if successful
//	AX=0 if not successful
//	  BL=080h -> function not implemented
//	  BL=081h -> VDISK is detected
//	  BL=0a2h -> handle is invalid
//	  BL=0abh -> handle is locked
#ifndef XXMS
#define RAX	%ax
#define RDX	%dx
#define RDI	%di
#else
#define RAX	%eax
#define RDX	%edx
#define RDI	%edi
#endif // XXMS
	
xms_free_xms:	
	call	xms_handle_valid
	jc	8f //@@xms_free_not_valid
	
	mov	%dx, %si		// Get handle into SI.

	xor	RDX, RDX		// Set to zero for cmp and mov below.
	
	cmpb	$1, xms_handle_used(%si)	// Is the block locked?
	jne	7f //@@locked		// Yes, bail out.

	cmp	RDX, xms_handle_xsize(%si)	// Is it a zero-length handle?
	je	6f //@@zero_handle

1:	//@@xms_free_loop:	
	mov	xms_handle_xbase(%si), RDI	// Get base address.
	add	xms_handle_xsize(%si), RDI	// Calculate end-address.

	call	xms_find_free_block	// Check free blocks.
	jc	5f //@@xms_free_done	// No, was last handle.
	
2:	//@@try_concat:
	cmp	xms_handle_xbase(%bx), RDI	// Is it adjacent to old block?
	jne	3f //@@try_concat_2

	mov	xms_handle_xsize(%bx), RAX	// Concat.
	add	RAX, RDI
	add	RAX, xms_handle_xsize(%si)

	mov	RDX, xms_handle_xbase(%bx)	// Blank handle.
	mov	RDX, xms_handle_xsize(%bx)

	jmp	4f //@@next

3:	//@@try_concat_2:	
	mov	xms_handle_xbase(%bx), RAX	// Is it adjacent to old block?
	add	xms_handle_xsize(%bx), RAX
	cmp	xms_handle_xbase(%si), RAX
	jne	4f //@@not_adjacent

	incb	xms_handle_used(%bx)		// This one in use temporaily.
	mov	xms_handle_xsize(%si), RAX	// Concat.
	add	RAX, xms_handle_xsize(%bx)
	mov	RDX, xms_handle_xbase(%si)	// Blank handle.
	mov	RDX, xms_handle_xsize(%si)
	mov	%dl, xms_handle_used(%si)	// Not in use anymore.
	mov	%bx, %si
	jmp	1b //@@xms_free_loop
	
4:	//@@next:	
	//@@not_adjacent:
	call	xms_find_next_free_block	// See if there are other blks.
	jnc	2b //@@try_concat

5:	//@@xms_free_done:
	mov	%dl, xms_handle_used(%si)	// Handle is not used anymore.
	ret
	
6:	//@@zero_handle:
	mov	RDX, xms_handle_xbase(%si)	// Blank handle.
	jmp	5b //@@xms_free_done

7:	//@@locked:
	mov	$XMS_BLOCK_LOCKED, %bl

8:	//@@xms_free_not_valid:
	pop	%ax
	ret

//-----------------------------------------------------------------------------
// moves an XMS block
// In:	AH=0bh
//	DS:SI=pointer to XMS move structure
// Out:	AX=1 if successful
//	AX=0 if not successful
//	  BL=080h -> function not implemented
//	  BL=081h -> VDISK is detected
//	  BL=082h -> A20 failure
//	  BL=0a3h -> source handle is invalid
//	  BL=0a4h -> source offset is invalid
//	  BL=0a5h -> destination handle is invalid
//	  BL=0a6h -> destination offset is invalid
//	  BL=0a7h -> length is invalid
//	  BL=0a8h -> move has invalid overlap
//	  BL=0a9h -> parity error

xms_move_xms:	
	cli				// no interrupts

	call	test_a20		// get A20 state
	pushf				// save it for later
	jnz	1f //@@a20_already_enabled	// We don't need to enable it if it's already is enabled.
	call	enable_a20		// now enable it!

1:	//@@a20_already_enabled:	
	mov	%es:xms_move_strc_len(%si), %ecx	// get length
	test	$1, %cl			// Is the length even?
	jnz	13f //@@invalid_length

	mov	%es:xms_move_strc_dest_handle(%si), %bx
	mov	%es:xms_move_strc_dest_offset(%si), %edx
	call	xms_get_move_addr	// get move address
	jc	14f //@@xms_move_failure_end
	mov	%ebx, %edi		// store in destination index

	mov	%es:xms_move_strc_src_handle(%si), %bx
	mov	%es:xms_move_strc_src_offset(%si), %edx
	call	xms_get_move_addr	// get move address
	jc	9f //@@src_not_valid
	mov	%ebx, %esi		// store in source index

	test	%ecx, %ecx		// Nothing to copy?
	jz	4f //@@xms_move_ok

	cmp	%edi, %esi		// src == dest?
	je	4f //@@xms_move_ok	// Yes, nothing to do.


bios_jmp_patch = .
bios_jmp_address_patch = .+1
bios_jmp_address_value = do_bios_move - . - 3
	
	// Check if we are in protected mode already.
	smsw	%ax
	test	$1, %al
	jnz	do_bios_move		// Already in PM.

	lgdt	gdt32			// load GDTR
	or	$1, %al			// set PE bit
	lmsw	%ax			// shazamm!
	ljmp	$code16idx, $to_pm	// flush IPQ and load CS sel.
to_pm:
	mov	$core32idx, %ax
	mov	%ax, %ds
	mov	%ax, %es

	cmp	%edi, %esi		// src <= dest?
	ja	2f //@@do_move		// No, go ahead.

	// src <= dest so we must copy from higher addresses to lower in case the ranges overlap.
	std				// High to low.
	addr32	lea	-4(%esi,%ecx), %esi	// Adjust addresses for dword size.
	addr32	lea	-4(%edi,%ecx), %edi
	shr	$2, %ecx		// Get number of DWORDS to move.
	jnc	3f //@@dword_boundary

	addr32	mov	2(%esi), %ax
	addr32	mov	%ax, 2(%edi)
	sub	$2, %esi		// We just moved a word so adjust
	sub	$2, %edi		// addresses for that.
	jmp	3f //@@dword_boundary

2:	//@@do_move:	
	shr	$2, %ecx		// get number of DWORDS to move
	jnc	3f //@@dword_boundary	// is length a DWORD multiple?
	
	addr32	movsw			// no, move first word to adjust
	
3:	//@@dword_boundary:
	rep	
	addr32	movsl			// now move the main block

	mov	%cr0, %eax
	and	$~1, %al		// clear PE bit
	mov	%eax, %cr0		// shazomm!

code_seg = .+3
	ljmp	$0, $to_rm		// flush IPQ and load CS sel.

to_rm:
	
4:	//@@xms_move_ok:	
	popf				// get A20 state
	jnz	5f //@@a20_was_enabled	// if it was enabled, do not disable
	call	disable_a20		// it was disabled, so restore state
	
5:	//@@a20_was_enabled:
	ret

do_bios_move:
	cmp	%edi, %esi		// Source > destination?
	ja	6f //@@bios_ok
	
	mov	%esi, %eax
	add	%ecx, %eax
	cmp	%edi, %eax
	ja	12f //@@illegal_overlap

6:	//@@bios_ok:
	push	%cs
	pop	%es			// ES == my segment.
		
	mov	%ecx, %edx
	shr	$1, %edx		// Length in words.
	
7:	//@@bios_move_loop:
	mov	%edx, %ecx
	cmp	$0x1000, %ecx		// 0x1000 is a good value according to Tom Ehlert.
	jbe	8f //@@length_ok

	mov	$0x1000, %ecx

8:	//@@length_ok:
	cli				// Protect bios_gdt for reentrancy.

	// Fill in source entries.
	mov	%esi, %eax
	mov	%ax, bios_src_low
	shr	$0x10, %eax
	mov	%al, bios_src_middle
	shr	$8, %eax
	mov	%al, bios_src_high

	// Fill in destination entries.
	mov	%edi, %eax
	mov	%ax, bios_dst_low
	shr	$0x10, %eax
	mov	%al, bios_dst_middle
	shr	$8, %eax
	mov	%al, bios_dst_high

	push	%ecx
	push	%esi

	mov	$bios_gdt, %si

	clc
	mov	$0x87, %ah
	int	$0x15
	sti

	pop	%esi
	pop	%ecx

	jc	11f //@@a20_failure

	addr32	lea	(%edi,%ecx,2), %edi	// dest += copied length.
	addr32	lea	(%esi,%ecx,2), %esi	// src += copied length.
	sub	%ecx, %edx		// length -= copied length.
	jnz	7b //@@bios_move_loop

	jmp	4b //@@xms_move_ok

9:	//@@src_not_valid:
	cmp	$XMS_INVALID_DESTINATION_HANDLE, %bl	// bl set by xms_get_move_addr.
	jne	13f //@@invalid_length
	mov	$XMS_INVALID_SOURCE_HANDLE, %bl
	jmp	14f //@@xms_move_failure_end
		
11:	//@@a20_failure:
	mov	$XMS_A20_FAILURE, %bl
	jmp	14f //@@xms_move_failure_end

12:	//@@illegal_overlap:
	mov	$XMS_OVERLAP, %bl
	jmp	14f //@@xms_move_failure_end

13:	//@@invalid_length:
	mov	$XMS_INVALID_LENGTH, %bl
	//  Fall through.
	
14:	//@@xms_move_failure_end:	
	popf				// Saved a20 state.
	jnz	15f //@@failure_a20_was_enabled	// if it was enabled, do not disable
	call	disable_a20		// it was disabled, so restore state
	
15:	//@@failure_a20_was_enabled:	
	pop	%ax
	ret

//-----------------------------------------------------------------------------
// locks an XMS block
// In:	AH=0ch
//	DX=XMS handle to be locked
// Out:	AX=1 if block is locked
//	  DX:BX=32-bit linear address of block
//	AX=0 if not successful
//	  BL=080h -> function not implemented
//	  BL=081h -> VDISK is detected
//	  BL=0a2h -> handle is invalid
//	  BL=0ach -> lock count overflow
//	  BL=0adh -> lock fails

xms_lock_xms:	
	call	xms_handle_valid
	jc	1f //@@xms_lock_not_valid
	
	mov	%dx, %bx
	incb	xms_handle_used(%bx)	// Increase lock count.
	jnz	2f //@@locked_successful	// go on if no overflow
	decb	xms_handle_used(%bx)	// Decrease lock count.
	mov	$XMS_LOCK_COUNT_OVERFLOW, %bl	// overflow, return with error
	
1:	//@@xms_lock_not_valid:	
	pop	%ax
	ret
	
2:	//@@locked_successful:
#ifndef XXMS
	movzwl	xms_handle_xbase(%bx), %eax	// Get block base address
#else // #ifdef XXMS
	mov	xms_handle_xbase(%bx), %eax	// Get block base address.
#endif // XXMS
	
	shl	$10, %eax		// Calculate linear address.
	mov	%ax, C_BX		// Store LSW.
	shr	$16, %eax
	mov	%ax, C_DX		// Store MSW.
	ret

//-----------------------------------------------------------------------------
// unlocks an XMS block
// In:	AH=0dh
//	DX=XMS handle to unlock
// Out:	AX=1 if block is unlocked
//	AX=0 if not successful
//	  BL=080h -> function not implemented
//	  BL=081h -> VDISK is detected
//	  BL=0a2h -> handle is invalid
//	  BL=0aah -> block is not locked

xms_unlock_xms:	
	call	xms_handle_valid
	jc	2f //@@xms_unlock_not_valid
	
	mov	%dx, %bx
	cmpb	$1, xms_handle_used(%bx)	// Check if block is locked.
	ja	1f //@@is_locked	// Go on if true.
	mov	$XMS_BLOCK_NOT_LOCKED, %bl
2:	//@@xms_unlock_not_valid:	
	pop	%ax
	ret
1:	//@@is_locked:
	decb	xms_handle_used(%bx)	// Decrease lock count.
	ret


//-----------------------------------------------------------------------------
// reallocates an UMB
// In:	AH=12h
//	BX=new size for UMB in paragraphs
//	DX=segment of UMB to reallocate
// Out:	AX=1 if successful
//	AX=0 if not successful
//	  BL=080h -> function not implemented
//	  BL=0b0h -> no UMB large enough to satisfy request
//	    DX=size of largest UMB in paragraphs
//	  BL=0b2h -> UMB segment is invalid
	
xms_realloc_umb:		// Not pretty but saves memory.
	// Fall through.

//-----------------------------------------------------------------------------
// requests an UMB block
// In:	AH=10h
//	DX=size of requested memory block in paragraphs
// Out:	AX=1 if successful
//	  BX=segment number of UMB
//	  DX=actual size of the allocated block in paragraphs
//	AX=0 if not successful
//	  DX=size of largest available UMB in paragraphs
//	  BL=080h -> function not implemented
//	  BL=0b0h -> only a smaller UMB are available
//	  BL=0b1h -> no UMBs are available

xms_request_umb:		// Not pretty but saves memory.
	movw	$0, C_DX	// No UMB memory.
	// Fall through.
	
//-----------------------------------------------------------------------------
// releases an UMB block
// In:	AH=11h
//	DX=segment of UMB
// Out:	AX=1 if successful
//	AX=0 if not successful
//	  BL=080h -> function not implemented
//	  BL=0b2h -> UMB segment number is invalid

xms_release_umb:	
	mov	$XMS_NOT_IMPLEMENTED, %bl
	pop	%ax
	ret

#ifndef XXMS
#include "xms_resident.inc"
#else // #ifdef XXMS
#include "xxms_resident.inc"
#endif // XXMS
	
#ifdef TRACE_CODE
trace_get_version:	
	.asciz	"get_version\r\n"
trace_request_hma:	
	.asciz	"request_hma\r\n"
trace_release_hma:	
	.asciz	"release_hma\r\n"
trace_global_enable_a20:	
	.asciz	"global_enable_a20\r\n"
trace_global_disable_a20:	
	.asciz	"global_disable_a20\r\n"
trace_local_enable_a20:	
	.asciz	"local_enable_a20\r\n"
trace_local_disable_a20:	
	.asciz	"local_disable_a20\r\n"
trace_query_a20:	
	.asciz	"query_a20\r\n"
trace_query_free_xms:	
	.asciz	"query_free_xms\r\n"
trace_alloc_xms:	
	.asciz	"alloc_xms\r\n"
trace_free_xms:	
	.asciz	"free_xms\r\n"
trace_move_xms:	
	.asciz	"move_xms\r\n"
trace_lock_xms:	
	.asciz	"lock_xms\r\n"
trace_unlock_xms:	
	.asciz	"unlock_xms\r\n"
trace_get_handle_info:	
	.asciz	"get_handle_info\r\n"
trace_realloc_xms:	
	.asciz	"realloc_xms\r\n"
trace_request_umb:	
	.asciz	"request_umb\r\n"
trace_release_umb:	
	.asciz	"release_umb\r\n"
trace_realloc_umb:	
	.asciz	"realloc_umb\r\n"
#ifdef XXMS
trace_query_any_free_xms:	
	.asciz	"query_any_free_xms\r\n"
trace_alloc_any_xms:	
	.asciz	"alloc_any_xms\r\n"
trace_get_extended_handle_info:	
	.asciz	"get_extended_handle_info\r\n"
trace_realloc_any_xms:	
	.asciz	"realloc_any_xms\r\n"
#endif // XXMS
#endif // TRACE_CODE

//-----------------------------------------------------------------------------
// XMS dispatcher
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// XMS dispatcher
// In:	AH - function number
// Out:	AX=0 -> function not supported
//	else see appr. routine

xms_table:	
	.short	xms_get_version, xms_request_hma
	.short	xms_release_hma, xms_global_enable_a20
	.short	xms_global_disable_a20, xms_local_enable_a20
	.short	xms_local_disable_a20, xms_query_a20
	.short	xms_query_free_xms, xms_alloc_xms
	.short	xms_free_xms, xms_move_xms
	.short	xms_lock_xms, xms_unlock_xms
	.short	xms_get_handle_info, xms_realloc_xms
	.short	xms_request_umb, xms_release_umb
	.short	xms_realloc_umb
#ifdef XXMS
	.short	xms_get_extended_handle_info, xms_realloc_any_xms
	.short	xms_query_any_free_xms, xms_alloc_any_xms
#endif // XXMS
	
#ifdef TRACE_CODE
trace_table:	
	.short	trace_get_version, trace_request_hma
	.short	trace_release_hma, trace_global_enable_a20
	.short	trace_global_disable_a20, trace_local_enable_a20
	.short	trace_local_disable_a20, trace_query_a20
	.short	trace_query_free_xms, trace_alloc_xms
	.short	trace_free_xms, trace_move_xms
	.short	trace_lock_xms, trace_unlock_xms
	.short	trace_get_handle_info, trace_realloc_xms
	.short	trace_request_umb, trace_release_umb
	.short	trace_realloc_umb
#ifdef XXMS
	.short	trace_get_extended_handle_info, trace_realloc_any_xms
	.short	trace_query_any_free_xms, trace_alloc_any_xms
#endif // XXMS
#endif // TRACE_CODE
	
xms_dispatcher:	
	jmp	dispatcher_entry
	nop				//
	nop				// guarantee hookability
	nop				//
dispatcher_entry:
	pushf				// save flags
	// Save registers.
	pushal				// AX and EAX at 32(%bp).
					// CX and ECX at 28(%bp).
					// DX and EDX at 24(%bp).
					// BL, BX and EBX at 20(%bp).
					// BH at 21(%bp)
					// (ESP at 16(%bp).)
					// (EBP at 12(%bp).)
					// SI at 8(%bp).
					// DI at 4(%bp).
	push	%ds			// 2
	push	%es			// 0
	mov	%sp, %bp
	
	movw	$1, C_AX		// Assume success.	

	// Set ES to caller's DS and DS to my DS.
	push	%ds
	pop	%es
	push	%cs
	pop	%ds
	cld

#ifndef XXMS
	cmp	$0x12, %ah		// Is it a supported function?
	ja	6f //@@not_supported
	
//	jmp	2f //@@supported
	
#else // #ifdef XXMS
	mov	%ah, %al
	cmp	$0x12, %al		// Is it a supported function?
	jbe	2f //@@supported
	
	// Not so simple method of mapping (0x88, 0x89, 0x8e, 0x8f) -> 
	// (0x15, 0x16, 0x13, 0x14) and failing anything else; but it's the
	// smallest. Note the order change.
	add	$2, %al			// Shift all entries up two steps
	and	$0xf807, %ax		// (mod 7). Check that bits 3 and 7
	cmp	$0x88, %ah		// are set and that bits 4-6 aren't.
	jne	6f //@@not_supported

	cmp	$3, %al			// Is it 0x8e, 0x8f, 0x88 or 0x89?
	ja	6f //@@not_supported

	add	$0x13, %al
//	jmp	@@supported
	
#endif // XXMS	

2:	//@@supported:	
	call	check_vdisk
	jz	5f //@@vdisk_installed
	
hook_patch:
	jmp	7f //@@hook_ints
3:	//@@hook_return:
#ifndef XXMS
	movzbw	%ah, %di
#else
	movzbw	%al, %di
#endif // XXMS
	
#ifdef TRACE_CODE
	// Check if a trace is wanted.
	bt	%di, trace
	jnc	4f //@@no_trace		//  No trace wanted.
	
	shl	$1, %di
	mov	trace_table(%di), %ax
	call	print_string
	jmp	42f // DI shifted.
	
4:	//@@no_trace:	
#endif // TRACE_CODE

	shl	$1, %di
42:	// DI shifted.
	pushw	$xms_exit_nok	
	call	*xms_table(%di)
	// Normally a successful XMS call returns here.
	pop	%ax			// Clean up failure return address.

xms_exit:
	pop	%es
	pop	%ds
	popal
	popf				// and Yury strikes again...
	lret

5:	//@@vdisk_installed:
	movb	$XMS_VDISK_DETECTED, %bl
	jmp	xms_exit_nok

6:	//@@not_supported:
	movb	$XMS_NOT_IMPLEMENTED, %bl	// everything else fails
	
	// All xms calls that fails returns here with error code in BL.
xms_exit_nok:
	movb	%bl, C_BL
	movw	$0, C_AX
	jmp	xms_exit
	
7:	//@@hook_ints:
#ifndef XXMS
	test	%ah, %ah		// non-version call?
#else
	test	%al, %al		// non-version call?
#endif	
	jz	3b //@@hook_return	// no, do not patch
	
	push	%eax
	push	%es
	
	pushw	$0
	pop	%es
	mov	%es:0x15*4, %eax
	mov	%eax, old_int15
	movl	$(0xdead0000+int15_handler), %es:0x15*4
hook_0x15_patch = . - 2
	
	pop	%es
	pop	%eax

10:	//@@end:
	movw	$0x9090, hook_patch	// insert two NOPs
	jmp	3b //@@hook_return	// and finish it

	
#ifdef TRACE_CODE
#include "print_string.inc"
#endif // TRACE_CODE
	
//-----------------------------------------------------------------------------
// Mark for the driver end. above has to be the resident part, below the
// transient.

	.globl	driver_end
driver_end:

//-----------------------------------------------------------------------------
// 16-bit transient code and data. only used once.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// checks if CPU is a 386
// In:	nothing
// Out:	CY=0 - processor is a 386 or higher
//	CY=1 - processor lower than 386
//	AX - destroyed.
//	DX - destroyed.
	
	.arch	i8086		// This part must be able to run on 8086.
check_cpu:	
	pushf
	push	%sp
	pop	%ax
	cmp	%sp, %ax
	jne	1f //@@not386
	mov	$0x7000, %ax
	push	%ax
	popf
	pushf
	pop	%ax
	test	$0x70, %ah
	jz	1f //@@not386
	popf
	clc
	ret
1:	//@@not386:
	popf
	stc
	ret
	.arch	i386		// Back to 386.

//-----------------------------------------------------------------------------
// Checks if A20 can be enabled and disabled.
// Out:	CF=0, ZF=1 - A20 switching works.
// 	CF=0, ZF=0 - Turning on A20 worked but cannot turn it off.
//	CF=1 - A20 failure

check_a20:	
	call	enable_a20
	call	test_a20		// TEST_A20 should return ZF=0
	jz	1f //@@a20failed
	call	disable_a20
	call	test_a20		// TEST_A20 should return ZF=1
	clc
	ret
1:	//@@a20failed:
	stc
	ret

//-----------------------------------------------------------------------------
// Saves a string pointed to by fs:ax into command_line. 
// command_line is truncated to a length of 255 bytes and upper cased.
// In:	FS:AX - pointer to string.
// Out:	AL - destroyed.

copy_command_line:	
	push	%di
	push	%si
	push	%cx

	mov	$XMS_COMMAND_LINE_LENGTH_MAX, %cx
	mov	%ax, %si
	mov	$command_line, %di
1:	//@@loop:
	mov	%fs:(%si), %al
	cmp	$'a', %al
	jb	2f //@@do_move
	cmp	$'z', %al
	ja	2f //@@do_move
	// Must be a lower case letter,
	add	$'A'-'a', %al		// which now is uppercase.
2:	//@@do_move:	
	mov	%al, (%di)
	dec	%cx
	jcxz	4f //@@too_long
	inc	%di
	inc	%si
	cmp	$0xa, %al
	je	4f //@@too_long	// Stop if we copied 0xa - DOZE have some interesting ideas of ending the command line.
	test	%al, %al
	jnz	1b //@@loop		// Stop if we did copy nul, else continue.

3:	//@@the_end:
	pop	%cx
	pop	%si
	pop	%di
	ret

4:	//@@too_long:
	movb	$0, (%di)		// Terminate command line.
	jmp	3b //@@the_end
		

	
//-----------------------------------------------------------------------------
// Analyses the contents of command_line and sets variables accordingly.
// In:	Nothing.
// Out:	AX - destroyed.
	
interpret_command_line:	
	push	%es
	push	%di
	push	%si
	push	%cx
	push	%bx

	mov	%ds, %ax
	mov	%ax, %es
	// First we must step over FDXMS.SYS, which we do by scaning for the first space, tab or ^M character.
	mov	$command_line, %si	// Remember where search started.
	mov	$XMS_COMMAND_LINE_LENGTH_MAX, %cx
	add	%si, %cx		// cx is guard.
	mov	$0x0920, %bx		// BH=tab, BL=space.
	mov	$0xd, %ah		// AH=^M.
	
1:	//@@look_again_for_white_space:	
	mov	(%si), %al
	cmp	%bl, %al
	je	2f //@@found_white_space
	cmp	%bh, %al
	je	2f //@@found_white_space
	cmp	%ah, %al
	je	2f //@@found_white_space
	inc	%si
	cmp	%cx, %si
	jae	24f //@@done
	jmp	1b //@@look_again_for_white_space
			
2:	//@@found_white_space:
	//@@next_arg:
	mov	$0x0d0a, %bx		// BH=^M, BL=^J
	mov	$0x0920, %ax		// AH=tab, AL=space
	call	eat_characters
	
	cmpb	$0, (%si)
	je	24f //@@done		// End of string?
	
	cmp	%cx, %si
	jae	24f //@@done		// Is this necessary?
	
	mov	%si, %bx		// Remember current position
	
#ifdef TRACE_CODE
	// TRACE argument.
	mov	$trace_str, %di
	call	strcmp
#ifdef DEBUG_CODE
	jc	6f //@@try_debug
#else
	jc	7f //@@try_bios
#endif // DEBUG_CODE
	
	push	%eax
	push	%ecx
	push	%edx
	cmpb	$'=', (%si)
	je	3f //@@trace_equal_sign
	mov	$0xffffffff, %eax
	jmp	4f //@@trace_no_value

3:	//@@trace_equal_sign:	
	inc	%si
	call	read_integer
	jc	5f //@@trace_expects_integer
	
4:	//@@trace_no_value:
	// Exchange bits 19-20 with 21,22.
	mov	%eax, %ecx
	mov	%eax, %edx
	and	$0xff87ffff, %eax
	and	$0x00600000, %ecx
	and	$0x00180000, %edx
	shr	$2, %ecx
	shl	$2, %edx
	or	%ecx, %eax
	or	%edx, %eax
	// Set trace mask.
	mov	%eax, trace
	pop	%edx
	pop	%ecx
	pop	%eax
	jmp	2b //@@next_arg

5:	//@@trace_expects_integer:
	pop	%edx
	pop	%ecx
	pop	%eax
	mov	$trace_no_int_str, %ax
	call	print_string
	jmp	2b //@@next_arg

#endif // TRACE_CODE

#ifdef DEBUG_CODE
	// DEBUG argument.
6:	//@@try_debug:	
	mov	%bx, %si
	
	mov	$debug_str, %di
	call	strcmp
	jc	7f //@@try_bios

	movb	$1, debug
	jmp	2b //@@next_arg	
#endif // DEBUG_CODE

	// BIOS argument.
7:	//@@try_bios:

#ifdef TRACE_CODE
	mov	%bx, %si
#else
#ifdef DEBUG_CODE
	mov	%bx, %si
#endif // DEBUG_CODE
#endif // TRACE_CODE
	
	mov	$bios_str, %di
	call	strcmp
	jc	8f //@@try_geode
	
//	or	$FLAG_USE_BIOS, flags
	movb	$0xe9, bios_jmp_patch	// 0xe9 is near jmp.
	movw	$bios_jmp_address_value, bios_jmp_address_patch
	jmp	2b //@@next_arg
	
	// GEODE argument.
8:	//@@try_geode:
	mov	%bx, %si
	mov	$geode_str, %di
	call	strcmp
	jc	9f //@@try_ps

	movb	$0xc3, delay_ret_patch	// 0xc3 is near ret.
	jmp	2b //@@next_arg
	
	// PS argument.
9:	//@@try_ps:
	mov	%bx, %si
	mov	$ps_str, %di
	call	strcmp
	jc	10f //@@try_delay

	movb	$0x02, enable_a20_out_value_patch // Patch value out.
	movb	$0x92, enable_a20_out_port_patch  // Patch out port address.
	movb	$0, disable_a20_out_value_patch	  // Patch value out.
	movb	$0x92, disable_a20_out_port_patch // Patch out port address.
	mov	$0x90, %al		// NOP.
	mov	$enable_a20_nop_start, %di
	mov	$(enable_a20_nop_end - enable_a20_nop_start), %cx
	rep	
	stosb
	mov	$disable_a20_nop_start, %di
	mov	$(disable_a20_nop_end - disable_a20_nop_start), %cx
	rep	
	stosb
	jmp	2b //@@next_arg

	// DELAY argument.
10:	//@@try_delay:
	mov	%bx, %si
	mov	$delay_str, %di
	call	strcmp
	jc	13f //@@try_numhandles
	
	push	%eax
	cmpb	$'=', (%si)
	jne	12f //@@delay_no_value_given

	inc	%si
	call	read_integer
	jc	12f //@@delay_no_value_given
	
	cmp	$0xffff, %eax
	jbe	11f //@@delay_value_ok

	mov	$delay_too_big_str, %ax
	call	print_string
	mov	$0xffff, %eax

11:	//@@delay_value_ok:	
	mov	%ax, xms_delay
	pop	%eax
	jmp	2b //@@next_arg	

12:	//@@delay_no_value_given:	
	mov	$delay_no_value_str, %ax
	call	print_string
	pop	%eax
	jmp	2b //@@next_arg

	// NUMHANDLES argument.
13:	//@@try_numhandles:
	mov	%bx, %si
	mov	$numhandles_str, %di
	call	strcmp
	jc	17f //@@try_int15

	push	%eax
	cmpb	$'=', (%si)
	jne	16f //@@numhandles_no_value_given

	inc	%si
	call	read_integer
	jc	16f //@@numhandles_no_value_given

	cmp	$2, %eax
	jae	14f //@@numhandles_at_least_two

	mov	$numhandles_too_small_str, %ax
	call	print_string
	mov	$2, %eax

14:	//@@numhandles_at_least_two:
	cmp	$NUM_HANDLES_MAX, %eax
	jbe	15f //@@numhandles_value_ok
	
	mov	$numhandles_too_big_str, %ax
	call	print_string
	mov	$NUM_HANDLES_MAX, %eax

15:	//@@numhandles_value_ok:
	mov	%ax, xms_num_handles
	pop	%eax
	jmp	2b //@@next_arg	

16:	//@@numhandles_no_value_given:	
	mov	$numhandles_no_value_str, %ax
	call	print_string
	pop	%eax
	jmp	2b //@@next_arg

	// INT15 argument
17:	//@@try_int15:	
	mov	%bx, %si
	mov	$int15_str, %di
	call	strcmp
	jc	21f //@@bad_arg

	push	%eax
	cmpb	$'=', (%si)
	jne	20f //@@int15_no_value_given

	inc	%si
	call	read_integer
	jc	20f //@@int15_no_value_given

	cmp	$64, %eax
	jae	18f //@@int15_hma_included

	mov	$int15_hma_not_included_str, %ax
	call	print_string
	xor	%eax, %eax
	
18:	//@@int15_hma_included:	
	cmp	$INT15_MAX, %eax
	jbe	19f //@@int15_value_ok

	mov	$int15_too_big_str, %ax
	call	print_string
	mov	$INT15_MAX, %ax

19:	//@@int15_value_ok:
	mov	%ax, int15_mem
	pop	%eax
	jmp	2b //@@next_arg

20:	//@@int15_no_value_given:	
	mov	$int15_no_value_str, %ax
	call	print_string
	pop	%eax
	jmp	2b //@@next_arg
		
	// Bad argument.
21:	//@@bad_arg:
	mov	$bad_arg_pre, %ax
	call	print_string
	mov	%bx, %si
	mov	$bad_arg_arg, %di
	
22:	//@@bad_arg_loop:	
	mov	(%si), %al
	mov	%al, (%di)
	test	%al, %al
	jz	23f //@@bad_arg_end
	cmp	$' ', %al
	je	23f //@@bad_arg_end
	cmp	$9, %al		// tab
	je	23f //@@bad_arg_end
	cmp	$0xa, %al	// lf
	je	23f //@@bad_arg_end
	cmp	$0xd, %al	// cr
	je	23f //@@bad_arg_end
	inc	%si
	inc	%di
	jmp	22b //@@bad_arg_loop
	
23:	//@@bad_arg_end:
	movb	$0, (%di)
	mov	$bad_arg_arg, %ax
	call	print_string
	mov	$bad_arg_post, %ax
	call	print_string	

	mov	(%si), %al
	test	%al, %al
	jz	24f //@@done
	
	inc	%si
	jmp	2b //@@next_arg

	// The end.
24:	//@@done:	
	pop	%bx
	pop	%cx
	pop	%si
	pop	%di
	pop	%es	
	ret	
	
#ifdef TRACE_CODE
trace_str:	
	.asciz	"TRACE"
trace_no_int_str:	
	.asciz	"TRACE expects an integer (e.g. TRACE=0xff)\r\n"
#endif // TRACE_CODE
	
numhandles_str:	
	.asciz	"NUMHANDLES"
numhandles_too_small_str:	
	.asciz	"Too small argument to NUMHANDLES increased to 2\r\n"
numhandles_too_big_str:	
	.asciz	"Too big argument to NUMHANDLES clamped down to 0x400\r\n"
numhandles_no_value_str:	
	.asciz	"NUMHANDLES expects an argument (e.g. NUMHANDLES=32)\r\n"
delay_str:	
	.asciz	"DELAY"
delay_too_big_str:	
	.asciz	"Too big argument to DELAY clamped down to 0xffff\r\n"
delay_no_value_str:	
	.asciz	"DELAY expects an argument (e.g. DELAY=5)\r\n"
bios_str:	
	.asciz	"BIOS"
debug_str:	
	.asciz	"DEBUG"
ps_str:	
	.asciz	"PS"
geode_str:	
	.asciz	"GEODE"
int15_str:	
	.asciz	"INT15"
int15_hma_not_included_str:	
	.asciz	"Too small argument to INT15 ignored.\r\n"
int15_too_big_str:	
	.ascii	"Too big argument to INT15 clamped down to ", INT15_MAX_STR
	.asciz	".\r\n"
int15_no_value_str:	
	.asciz	"INT15 expects an argument (e.g. INT15=0x800).\r\n"
bad_arg_pre:	
	.asciz	"Ignoring invalid option: "
bad_arg_post:	
	.byte	13,10,0
bad_arg_arg:	
	.space	XMS_COMMAND_LINE_LENGTH_MAX, 0

//-----------------------------------------------------------------------------
// Reads an integer from a string pointed ot by SI.
// In:	SI - pointers to string.
// Out:	CY=0 - successful conversion.
//	  SI - updated.
//	  EAX - integer read.
//	CY=1 - First character was not a digit.
//	  EAX - destroyed.
		
read_integer:	
	push	%edx
	xor	%eax, %eax
	mov	(%si), %al
	cmp	$0, %al
	je	9f //@@failure
	cmp	$'0', %al
	jb	9f //@@failure
	cmp	$'9', %al
	ja	9f //@@failure
	sub	$'0', %al
	movzbl	%al, %edx
	inc	%si
	mov	(%si), %al
	cmp	$'X', %al
	je	3f //@@perhaps_hex_number
	
1:	//@@decimal_loop:	
	cmp	$'0', %al
	jb	2f //@@done
	cmp	$'9', %al
	ja	2f //@@done
	addr32	lea	-'0'/2(%edx,%edx,4), %edx	// Luckily zero and '0' are even numbers!
	shl	$1, %edx
	add	%eax, %edx
	inc	%si
	mov	(%si), %al
	cmp	$0, %al
	je	2f //@@done
	jmp	1b //@@decimal_loop
	
2:	//@@done:
	mov	%edx, %eax
	pop	%edx
	clc
	ret

3:	//@@perhaps_hex_number:
	cmp	$0, %dx
	jne	2b //@@done

	inc	%si
	mov	(%si), %al
	cmp	$0, %al
	je	8f //@@looked_like_hex_but_was_not
4:	//@@hex_number:	
	cmp	$'0', %al
	jb	8f //@@looked_like_hex_but_was_not
	cmp	$'F', %al
	ja	8f //@@looked_like_hex_but_was_not
	cmp	$'9', %al
	jbe	5f //@@digit
	cmp	$'A', %al
	jb	8f //@@looked_like_hex_but_was_not
	add	$('0'-'A'+10), %al	// Sets al to the ASCII code for the
					// characters after '9'.
5:	//@@digit:
	sub	$'0', %al
	movzbl	%al, %edx
	inc	%si
	mov	(%si), %al
6:	//@@hex_loop:	
	cmp	$'0', %al
	jb	2b //@@done
	cmp	$'F', %al
	ja	2b //@@done
	cmp	$'9', %al
	jbe	7f //@@another_digit
	cmp	$'A', %al
	jb	2b //@@done
	add	$('0'-'A'+10), %al	// Sets al to the ASCII code for the
					// characters after '9'.
7:	//@@another_digit:
	sub	$'0', %al
	shl	$4, %edx
	add	%eax, %edx
	inc	%si
	mov	(%si), %al
	cmp	$0, %al
	je	2b //@@done
	jmp	6b //@@hex_loop
	
8:	//@@looked_like_hex_but_was_not:
	dec	%si
	jmp	2b //@@done

9:	//@@failure:
	pop	%edx
	stc
	ret
	
//-----------------------------------------------------------------------------
// Increases SI until a character that do not match contents of AH, AL, BH or BL is found.
// In:	SI - pointer to string.
//	AL - character to step over
//	AH - character to step over, 0x0 to ignore and to ignore BX contents.
//	BL - character to step over, 0x0 to ignore and to ignore BH contents.
//	BH - character to step over, 0x0 to ignore.
// Out:	SI - updated
	
eat_characters:	
1:	//@@loop:	
	cmp	(%si), %al
	jne	2f //@@try_ah
	inc	%si
	jmp	1b //@@loop
2:	//@@try_ah:
	test	%ah, %ah
	jz	5f //@@done
	cmp	(%si), %ah
	jne	3f //@@try_bl
	inc	%si
	jmp	1b //@@loop
3:	//@@try_bl:
	test	%bl, %bl
	jz	5f //@@done
	cmp	(%si), %bl
	jne	4f //@@try_bh
	inc	%si
	jmp	1b //@@loop
4:	//@@try_bh:
	test	%bh, %bh
	jz	5f //@@done
	cmp	(%si), %bh
	jne	5f //@@done
	inc	%si
	jmp	1b //@@loop

5:	//@@done:	
	ret
	
//-----------------------------------------------------------------------------
// Compares two strings up to DI points at nul.
// In:	DI, SI - pointers to strings to compare.
// Out:	CY=0 - strings equal
//	  DI - points at null
//	  SI - points at character where DI is null.
//	  AX - destroyed
//	CY=1 - strings not equal
//	  DI - points at character which diffs
//	  SI - points at character which diffs
//	  AX - destroyed
		
strcmp:	
1:	//@@loop:	
	mov	(%di), %al
	cmp	$0, %al			// Is it nul?
	je	3f //@@done		// Yes, we have a match!
	
	cmp	(%si), %al
	jne	2f //@@failure		// Not equal so no match.
	inc	%si
	inc	%di
	jmp	1b //@@loop

2:	//@@failure:	
	stc
	ret
	
3:	//@@done:
	clc
	ret		

//-----------------------------------------------------------------------------
// initializes the driver. called only once!
// may modify DI
// In:	Nothing.
// Out:	DS - destroyed <--- N.B.!!!
//	DI - destroyed.

init_message:	
	.byte	13,10
	.space	80, 0xc4
	.ascii	"FreeDOS XMS-Driver\r\n"
	.ascii	"Copyright (c) 1995, Till Gerken\r\n"
	.ascii	"Copyright 2001-2005, Martin Str�mberg et al. (see HISTORY.TXT)\r\n\r\n"
	.ascii	"Driver Version: ", DRIVER_VERSION, " \t"
	.ascii	"Interface Version: ", INTERFACE_VERSION, "\r\n"
	.ascii	"Information: ", INFO_STR, "\r\n$"

old_dos:	
	.ascii	"XMS needs at least DOS version 3.00.$"
xms_twice:	
	.ascii	"XMS is already installed.$"
vdisk_detected:	
	.ascii	"VDISK has been detected.$"
no_386:	
	.ascii	"At least a 80386 is required.$"
a20_error:	
	.ascii	"Unable to switch on A20 address line.$"
a20_not_off:	
	.ascii	"Unable to switch off A20 address line.\r\n$"
xms_memory_hole:	
	.ascii	"Memory hole below 16MiB detected. This release of FDXMS does not support\r\n"
	.ascii	"this. Using only memory below 16MiB. Please contact the maintainer!\r\n$"
xms_sizeerr:	
	.ascii	"Unable to determine size of extended memory.$"
xms_toosmall:	
	.ascii	"Extended memory is too small or not available.$"
int15_size_no_xms:	
	.ascii	"INT15 value clamped down to the size of the extended memory block that starts\r\n"
	.ascii	"at 0x110000 minus 1.\r\n$"
	
error_msg:	
	.ascii	" Driver won't be installed."
	.byte	7,13,10,'$'
	
command_line:	
	.space	XMS_COMMAND_LINE_LENGTH_MAX+1, 0
	
	.arch	i8086		// This part must be able to run on 8086.
initialize:	
	pushf
	push	%es
	push	%si
	push	%ax
	push	%bx
	push	%cx
	push	%dx

	push	%ds			// Put DS into ES.
	pop	%es
	push	%cs			// Put my segment into DS.
	pop	%ds
	
	cld

	mov	$9, %ah			// first, welcome the user!
	mov	$init_message, %dx
	int	$0x21

	mov	$0x3000, %ax		// get DOS version number
	int	$0x21
	xchg	%ah, %al		// convert to BSD
	cmp	$0x300, %ax		// we need at least 3.00
	mov	$old_dos, %dx
	jb	15f //@@error_exit

	mov	$0x4300, %ax		// check if XMS is already
	int	$0x2f			// installed
	cmp	$0x80, %al
	mov	$xms_twice, %dx
	je	15f //@@error_exit

	call	check_cpu		// do we have at least a 386?
	mov	$no_386, %dx
	jc	15f //@@error_exit

	.arch	i386			// Now we know we have at least a 386.
	pop	%dx			// Restore all saved registers except
	pop	%cx			// si, 
	pop	%bx		
	pop	%ax
	push	%eax			// and save the 32 bit variants instead
	push	%ebx
	push	%ecx
	push	%edx
	push	%fs			// and FS.
	
	// Check command line. It is very important this is done early
	// as it modifies A20 line toggling.
	lfs	%es:init_strc_cmd_line(%di), %ax
	call	copy_command_line
	call	interpret_command_line
		
	call	check_a20		// check A20
	mov	$a20_error, %dx
	jc	13f //@@error_exit_386

	jz	1f //@@a20_off

 	// We managed to turn it on (already on?) but not turning it off.
	mov	$9, %ah
	mov	$a20_not_off, %dx
	int	$0x21

1:	//@@a20_off:
	call	check_vdisk		// is VDISK installed?
	mov	$vdisk_detected, %dx
	jz	13f //@@error_exit_386
	
	// SI points to start of XMS handle table.
	mov	$driver_end, %si

	// Find memory.
	
#ifdef XXMS
	call	int15ax0xe820		// Try INT15 EAX=0xe820 first.
	
	test	%eax, %eax		// If INT15 EAX=0xe820 did find any 
	jnz	8f //@@reserve_int15	// memory, there is no need to try the
					// other functions.
#endif // XXMS

2:	//@@try_int15_axe801:	
	clc
	mov	$0xe801, %ax
	int	$0x15
	jc	5f //@@try_int15_ah88

	mov	%ax, %di
	or	%bx, %di		// If both AX and BX are not zero use the values.
	jnz	3f //@@use_ax_bx

	// AX and BX was zero, use CX and DX instead.
	mov	%dx, %bx
	mov	%cx, %ax
	
3:	//@@use_ax_bx:
	cmp	$0x3c00, %ax
	je	4f //@@no_memory_hole

	test	%bx, %bx
	jz	4f //@@no_memory_hole
	
	// We have a hole in memory, just below 16MiB. 
	mov	%ax, %bx		// Save AX.
	mov	$9, %ah
	mov	$xms_memory_hole, %dx
	int	$0x21
	mov	%bx, %ax		// Restore AX.
	xor	%bx, %bx		// Use only memory below 16MiB.
	
4:	//@@no_memory_hole:
	movzwl	%ax, %eax
	movzwl	%bx, %ebx
	shl	$6, %ebx
	add	%ebx, %eax
	
	jmp	6f //@@found_size

5:	//@@try_int15_ah88:	
	clc
	mov	$0x88, %ah		// extended memory size
	int	$0x15
	mov	$xms_sizeerr, %dx
	jc	13f //@@error_exit_386
	and	$0xffff, %eax
	
6:	//@@found_size:	
	mov	$xms_toosmall, %dx
	sub	$64, %eax		// Save HIMEM area.
	jc	13f //@@error_exit_386	// If there are not 64ki,
					// there is nothing to do.
	
#ifndef XXMS
	cmp	$XMS_MAX, %eax
	jbe	7f //@@fill_first_entry

	mov	$9, %ah
	mov	$xms_size_clamped, %dx
	int	$0x21
	mov	$XMS_MAX, %eax
		
7:	//@@fill_first_entry:	
	movw	$XMS_START, xms_handle_xbase(%si)	// Init first block and
	mov	%ax, xms_handle_xsize(%si)	// give it all 
					// available memory.
	movzwl	%ax, %eax		// Zero out bits 16-31.
#else // #ifdef XXMS
	addr32	lea	XMS_START(%eax), %ecx	// Calculate ending address
	shl	$10, %ecx
	dec	%ecx
	mov	%ecx, xms_highest_address	// and store it.
	
	movl	$XMS_START, xms_handle_xbase(%si)	// Init first block and
							// give it all
	mov	%eax, xms_handle_xsize(%si)		// available memory.

#endif // XXMS
	
	movb	$0, xms_handle_used(%si)	// Handle not used or locked.
	add	$SIZE_xms_handle, %si

	shl	$10, %eax		// Convert memory size to bytes.

8:	//@@reserve_int15:
	push	%eax			// Save EAX.
#ifdef XXMS
	movl	$0, xms_handle_xsize(%si)	// Mark end of initialised handles.
	movzwl	int15_mem, %eax	// Should any memory be left for INT15?
#else
	movw	$0, xms_handle_xsize(%si)	// Mark end of initialised handles.
	mov	int15_mem, %ax		// Should any memory be left for INT15?
#endif
	test	%ax, %ax		// 16-bit value in 32-bit register.
	jz	12f //@@print_memory_size	// No INT15 reservation.
	
	// Reserve space for INT15 reserved memory.
	mov	$driver_end, %si 	// SI points to first XMS handle.
	sub	$64, RAX
	cmp	xms_handle_xsize(%si), RAX
	jb	11f //@@int15_size_ok

9:	//@@int15_size_nok:
	mov	$9, %ah
	mov	$int15_size_no_xms, %dx
	int	$0x21

	mov	xms_handle_xsize(%si), RAX
	dec	RAX
	cmp	$INT15_MAX, RAX
	jbe	10f //@@size_below_int15_max
	
	mov	$INT15_MAX, RAX
	
10:	//@@size_below_int15_max:	
	mov	%ax, int15_mem		// RAXs bits 16-31 guaranteed to be zero here.

11:	//@@int15_size_ok:
	add	RAX, xms_handle_xbase(%si)
	sub	RAX, xms_handle_xsize(%si)

12:	//@@print_memory_size:		
	pop	%eax			// Restore EAX.

	// EAX contains the size of XMS in bytes now. Print what we have found.
	mov	%eax, %ebx		// Save EAX.
	call	print_hex_number
	mov	$9, %ah
	mov	$xms_mem_found_middle, %dx
	int	$0x21
	mov	%ebx, %eax
	call	print_dec_number
	mov	$9, %ah
	mov	$xms_mem_found_post, %dx
	int	$0x21

	
	mov	%cs, %ax		// setup descriptors
	mov	%ax, code_seg		// eliminate relocation entry
	movzwl	%ax, %eax
	shl	$4, %eax
	or	%eax, code16dsc+2
	add	%eax, gdt32+2

	// Patch my CS into INT0x15 hooking code.
	mov	%cs, %ax
	mov	%ax, hook_0x15_patch
	
	mov	$0x352f, %ax		// Get INT2Fh vector.
	int	$0x21
	mov	%es, old_int2f+2
	mov	%bx, old_int2f

	mov	$0x252f, %ax		// install own INT2Fh
	mov	$int2f_handler, %dx
	int	$0x21

	les	request_ptr, %di
	mov	%cs, %es:2+init_strc_end_addr(%di)	// set end address
	xor	%dx, %dx
	mov	$SIZE_xms_handle, %ax
	mulw	xms_num_handles
	add	$driver_end, %ax
	mov	%ax, %es:init_strc_end_addr(%di)
	movl	$0, %es:init_strc_cmd_line(%di)	// RBIL says this should be zeroed.
	movw	$STATUS_DONE, %es:request_hdr_status(%di)	// we are alright
	orb	$FLAG_INITIALISED, flags	// Mark that we have initialised.
	mov	$9, %ah
	mov	$init_finished, %dx
	int	$0x21

	// Zero out empty handles.
	// First calculate how much to zero.
	mov	%es:init_strc_end_addr(%di), %ax	// AX = end of driver.
	mov	%ax, %cx
	sub	zero_out_region_start, %cx	// CX = n. bytes to zero out.

	// Setup registers for zeroing.
	push	%ds
	pop	%es
	mov	zero_out_region_start, %di

	// Check if reallocation is necessary.
	cmp	$1000f, %ax
	jbe	1000f // zero_out_code_start

	// Rellocation.
	push	%cx
	push	%di
	std
	mov	$(1001f - 1000f), %cx
	mov	$(1001f - 1), %si
	mov	%ax, %di
	dec	%di
	add	%cx, %di
	rep
	movsb
	cld

	pop	%di
	pop	%cx
	
	jmp	*%ax
	
1000:	// zero_out_code_start
	xor	%al, %al
	rep
	stosb
	
14:	//@@exit_386:
	pop	%fs
	pop	%edx
	pop	%ecx
	pop	%ebx
	pop	%eax

	.arch	i8086		// This part must be able to run on 8086.
16:	// exit:	
	pop	%si
	pop	%es
	popf
	ret
1001:	// zero_out_code_end
	
	.arch	i386		// Back to 386.
13:	//@@error_exit_386:
	les	request_ptr, %di
	mov	%cs, %es:2+init_strc_end_addr(%di)	// set end address
	movw	$0, %es:init_strc_end_addr(%di)	// now, we do not use
						// any space
	movl	$0, %es:init_strc_cmd_line(%di)	// RBIL says this should be zeroed.
	movw	$(STATUS_ERROR|STATUS_DONE), %es:request_hdr_status(%di)	// waaaah!
	mov	$9, %ah			// print msg
	int	$0x21
	mov	$error_msg, %dx
	int	$0x21
	mov	$9, %ah
	mov	$init_finished, %dx
	int	$0x21
	jmp	14b //@@exit_386
	
	.arch	i8086		// This part must be able to run on 8086.
15:	//@@error_exit:
	les	request_ptr, %di
	mov	%cs, %es:2+init_strc_end_addr(%di)	// set end address
	movw	$0, %es:init_strc_end_addr(%di)	// now, we do not use
							// any space
	movw	$0, %es:2+init_strc_cmd_line(%di)	// RBIL says this should be zeroed.
	movw	$0, %es:init_strc_cmd_line(%di)
	movw	$(STATUS_ERROR|STATUS_DONE), %es:request_hdr_status(%di)	// waaaah!
	mov	$9, %ah			// print msg
	int	$0x21
	mov	$error_msg, %dx
	int	$0x21
	mov	$9, %ah
	mov	$init_finished, %dx
	int	$0x21
	pop	%dx
	pop	%cx
	pop	%bx
	pop	%ax
	jmp	16b //exit
	
	.arch	i386		// Back to 386.

//-----------------------------------------------------------------------------
// Prints a hexadecimal number, using INT21, AH=0x9.
// In:	EAX - the number to be printed.
//	DS - initialized with code segment
number:	
	.ascii	"0x????????$"
print_hex_number:	
	push	%bx
	push	%cx
	push	%edx
	push	%eax
	mov	$(number + 2), %bx
	mov	$28, %cl
	mov	%eax, %edx

1:	//@@loop:	
	mov	%edx, %eax
	shr	%cl, %eax
	and	$0xf, %al
	cmp	$0xa, %al
	jb	2f //@@digit
	add	$('A'-'0'-10), %al
2:	//@@digit:
	add	$'0', %al
	mov	%al, (%bx)
	inc	%bx
	sub	$4, %cl
	jb	3f //@@print
	jmp	1b //@@loop

3:	//@@print:
	mov	$number, %dx
	mov	$9, %ah
	int	$0x21
	
	pop	%eax
	pop	%edx
	pop	%cx
	pop	%bx
	ret

	
//-----------------------------------------------------------------------------
// Prints a decimal number, using INT21, AH=0x9.
// In:	EAX - the number to be printed.
//	DS - initialized with code segment
dec_number:	
	.ascii	"429496729"
dec_num_start:	
	.ascii	"6$"
print_dec_number:	
	push	%bx
	push	%edx
	push	%ecx
	push	%eax
	mov	$dec_num_start, %bx
	mov	$10, %ecx

1:	//@@loop:
	xor	%edx, %edx
	div	%ecx
	add	$'0', %dl
	mov	%dl, (%bx)
	test	%eax, %eax		// Any digits left?
	jz	2f //@@print
	dec	%bx
	jmp	1b //@@loop

2:	//@@print:
	mov	%bx, %dx
	mov	$9, %ah
	int	$0x21
	
	pop	%eax
	pop	%ecx
	pop	%edx
	pop	%bx
	ret

	
#ifdef XXMS
#include "xxms_nonresident.inc"	
#endif // XXMS

	
#ifndef TRACE_CODE
#include "print_string.inc"
#endif // TRACE_CODE	
	
//-----------------------------------------------------------------------------
// Some variables that must survive initial memory block fills.
zero_out_region_start:	
	.short	driver_end + SIZE_xms_handle
		
xms_size_clamped:	
	.ascii	"Size clamped down to 64MB (0xfbc0 KiB to be precise).\r\n$"
	
init_finished:	
	.space	80, 0xc4
	.byte	13,10,'$'

xms_mem_found_middle:	
	.ascii	" ($"
xms_mem_found_post:	
	.ascii	" decimal) bytes extended memory detected\r\n$"
	
//******************************************************************************

	.end	
