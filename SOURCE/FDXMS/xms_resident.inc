// Copyright 2001-2005, Martin Str�mberg.
//-----------------------------------------------------------------------------
// returns free XMS
// In:	AH=8
// Out:	AX=size of largest free XMS block in kbytes
//	DX=total amount of free XMS in kbytes
//	(BL=0 if ok)
//	(BL=080h -> function not implemented)
//	(BL=081h -> VDISK is detected)
//	BL=0a0h -> all XMS is allocated

xms_query_free_xms:
	mov	xms_num_handles, %cx
	mov	$driver_end, %bx
	
	xor	%ax, %ax			// Contains largest free block.
	xor	%dx, %dx			// Contains total free XMS.

1:	//@@check_next:
	cmpb	$0, xms_handle_used(%bx)
	jne	2f //@@in_use
	
	mov	xms_handle_xsize(%bx), %si	// Get size.
	add	%si, %dx			// Update total amount.
	cmp	%ax, %si			// Is this block bigger than what we've found so far?
	jbe	2f //@@not_larger
	
	mov	%si, %ax			// Larger, update.
	
2: 	//@@in_use:
	//@@not_larger:
	add	$SIZE_xms_handle, %bx
	loop	1b //@@check_next

	mov	%dx, C_DX
	mov	%ax, C_AX
	test	%ax, %ax		// Is there any free memory?
	jz	3f
	ret
	
3:
	mov	$XMS_ALREADY_ALLOCATED, %bl
	pop	%ax
	ret

//-----------------------------------------------------------------------------
// allocates an XMS block
// In:	AH=9
//	DX=amount of XMS being requested in kbytes
// Out:	AX=1 if successful
//	  DX=handle
//	AX=0 if not successful
//	  BL=080h -> function not implemented
//	  BL=081h -> VDISK is detected
//	  BL=0a0h -> all XMS is allocated
//	  BL=0a1h -> no free handles left

xms_alloc_xms:
	call	xms_find_free_block	// See if there's a free block.
	jc	6f //@@no_free_memory	// If there isn't, fail.
	jmp	2f //@@check_size
	
1:	//@@get_next_block:
	call	xms_find_next_free_block
	jc	6f //@@no_free_memory
2:	//@@check_size:
	cmp	xms_handle_xsize(%bx), %dx	// check if it's large enough
	ja	1b //@@get_next_block		// no, get next block

	mov	%bx, %si			// save handle address
	incb	xms_handle_used(%bx)	// this block is used from now on

3:	//@@find_handle:	
	call	xms_find_free_handle	// see if there's a blank handle
	jc	5f //@@perfect_fit		// no, there isn't, alloc all mem left
	mov	xms_handle_xsize(%si), %ax	// get size of old block
	sub	%dx, %ax			// calculate resting memory
	jz	5f //@@perfect_fit			// if it fits perfectly, go on
	mov	%ax, xms_handle_xsize(%bx)	// store sizes of new blocks
	mov	%dx, xms_handle_xsize(%si)
	mov	xms_handle_xbase(%si), %ax	// get base address of old block
	add	%dx, %ax			// calculate new base address
	mov	%ax, xms_handle_xbase(%bx)	// store it in new handle

5:	//@@perfect_fit:
	mov	%si, C_DX			// Return handle in C_DX.

	// Success.
	ret
	
6:	//@@no_free_memory:
	// If no memory was asked for, just allocate a handle.
	test	%dx, %dx
	jz	7f //@@zero_size_allocation
	
	mov	$XMS_ALREADY_ALLOCATED, %bl
	jmp	9f //@@failure_epilogue

7:	//@@zero_size_allocation:
	call	xms_find_free_handle	// see if there's a blank handle
	jc	8f //@@no_handles_left	// No, there isn't, fail.

	mov	%bx, %si			// Save handle address.
	
	// We have the handle. Mark it as used.
	incb	xms_handle_used(%bx)
	jmp	5b //@@perfect_fit

8:	//@@no_handles_left:
	mov	$XMS_NO_HANDLE_LEFT, %bl
	
9:	//@@failure_epilogue:	
	pop	%ax
	ret

//-----------------------------------------------------------------------------
// returns XMS handle information
// In:	AH=0eh
//	DX=XMS block handle
// Out:	AX=1 if successful
//	  BH=block's lock count
//	  BL=number of free XMS handles
//	  DX=block's length in kbytes
//	AX=0 if not successful
//	  BL=080h -> function not implemented
//	  BL=081h -> VDISK is detected
//	  BL=0a2h -> handle is invalid

xms_get_handle_info:
	call 	xms_handle_valid
	jc	4f //@@not_valid

	push	%dx				// Save handle for later.

	xor	%ax, %ax		// ax is number of free handles.

	// Setup for loop.
	mov	$driver_end, %bx
	mov	xms_num_handles, %cx
	
1:	//@@look_again:
	cmpb	$0, xms_handle_used(%bx)	// In use?
	jne	2f //@@add_some			// Yes, go to next.
	inc	%ax				// No, one more free handle.

2: 	//@@add_some:
	add	$SIZE_xms_handle, %bx
	loop	1b //@@look_again

	//  Now ax contains number of free handles.
	cmp	$0x100, %ax	// Make sure that we don't overflow C_BL.
	jb	3f //@@less_than_256
	mov	$0xff, %al
3:	//@@less_than_256:	
	
	pop 	%bx 				// Get handle saved earlier.
	mov	xms_handle_used(%bx), %ah	// Get lock count.
	dec	%ah
	mov	%ax, C_BX	// Store number of free handles and lock count.
	mov	xms_handle_xsize(%bx), %ax	// Get block size.
	mov	%ax, C_DX			// Store block size.
	ret

4: 	//@@not_valid:	
	pop	%ax
	ret

//-----------------------------------------------------------------------------
// reallocates an XMS block. only supports shrinking.
// In:	AH=0fh
//	BX=new size for the XMS block in kbytes
//	DX=unlocked XMS handle
// Out:	AX=1 if successful
//	AX=0 if not successful
//	  BL=080h -> function not implemented
//	  BL=081h -> VDISK is detected
//	  BL=0a0h -> all XMS is allocated
//	  BL=0a1h -> all handles are in use
//	  BL=0a2h -> invalid handle
//	  BL=0abh -> block is locked

xms_realloc_xms:
	call	xms_handle_valid
	jc	4f //@@xms_realloc_not_valid
	
	xchg	%bx, %dx
	cmp	xms_handle_xsize(%bx), %dx
	jbe	2f //@@shrink_it

1:	//@@no_xms_handles_left:
	mov	$XMS_NO_HANDLE_LEFT, %bl		// simulate a "no handle" error
4:	//@@xms_realloc_not_valid:	
	pop	%ax
	ret

2:	//@@shrink_it:
	mov	%bx, %si
	call	xms_find_free_handle		// get blank handle
	jc	1b //@@no_xms_handles_left		// return if there's an error
	mov	xms_handle_xsize(%si), %ax	// get old size
	mov	%dx, xms_handle_xsize(%si)
	sub	%dx, %ax			// calculate what's left over
	jz	3f //@@dont_need_handle		// skip if we don't need it
	add	xms_handle_xbase(%si), %dx	// calculate new base address
	mov	%dx, xms_handle_xbase(%bx)	// store it
	mov	%ax, xms_handle_xsize(%bx)	// store size
	movb	%cl, xms_handle_used(%bx)	// Block is not locked nor used.
3:	//@@dont_need_handle:
	ret
